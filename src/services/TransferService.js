import * as transferHistoryData from './transfer-history.json';

const transferList = [];
const transferTemplateList = [];
let currentId = 0;

const transferService = {
  addTransfer(newTransfer) {
    currentId += 1;
    transferList.push(Object.assign({}, newTransfer, { id: currentId }));
  },

  addTransferTemplate(newTransferTemplate) {
    transferTemplateList.push(newTransferTemplate);
  },

  getTransferList() {
    return transferList;
  },

  getTransferTemplateList() {
    return transferTemplateList;
  },
};

function init() {
  transferHistoryData.transfers.forEach(item => transferService.addTransfer(item));
}

init();

export default transferService;
