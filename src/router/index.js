import Vue from 'vue';
import Router from 'vue-router';
import Transfer from '@/components/Transfer';
import History from '@/components/History';
import TransferTemplate from '@/components/TransferTemplate';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Transfer',
      component: Transfer,
    },
    {
      path: '/history',
      name: 'History',
      component: History,
    },
    {
      path: '/transfer-template',
      name: 'TransferTemplate',
      component: TransferTemplate,
    },
  ],
});
